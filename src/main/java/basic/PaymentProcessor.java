package basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan
public class PaymentProcessor {

    @Autowired
    private PaymentMethod cashPayment;

    public PaymentProcessor() {
        super();
    }


    public static void main(String[] args) {
        SpringApplication.run(PaymentProcessor.class);
    }

    @Bean
    public PaymentMethod bitCoinPayment() {
        return new BitCoinPayment();
    }


    @Bean
    public PaymentMethod cashPayment() {
        return new CashPayment();
    }
}
